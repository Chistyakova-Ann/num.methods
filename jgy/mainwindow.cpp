#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

const double a=0;
const double b=5;
const double h=0.001;
const double o=-10;
const double l=10;
int p = (int)((b - a) / h);

double MainWindow::f(double t, double n) {
    return  tan(t)+n;
  //  return t+n;
 }

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::P_Eiler(){

    double *n, *t;
    int i;
    t = new double[p + 1];
    n = new double[p + 1];

    QVector<double> k(p+1), d(p+1);

    t[0] = 0; n[0] = 0;
    for (i = 1; i <= p; i++){
        t[i] = a + i*h;
        n[i] = n[i - 1] + h*f(t[i - 1], n[i - 1]);
        k[i]=t[i];
        d[i]=n[i];
   }
   ui->widget->addGraph();
   ui->widget->graph()->setData(k, d);
   ui->widget->xAxis->setLabel("t");
   ui->widget->yAxis->setLabel("n");
   ui->widget->xAxis->setRange(a,b);
   ui->widget->yAxis->setRange(o,l);
   ui->widget->replot();
}

void MainWindow::V_Eiler(){

    double *n, *t,*c;
    int i;
    t = new double[p + 1];
    n = new double[p + 1];
    c = new double[p + 1];

    QVector<double> k(p+1), d(p+1);

    t[0] = 0; n[0] = 0;
    for (i = 1; i <= p; i++){
        t[i] = a + i*h;
        k[i]=t[i];
        if(i<2){
            n[i] = n[i - 1] + h*f(t[i - 1], n[i - 1]);
            d[i]=n[i];
        }
        else{
            n[i] = n[i - 1] + h*f(t[i - 1], n[i - 1]);
            c[i] = n[i - 1] + h*f(t[i], n[i]);
            d[i]=c[i];
        }
    }
    ui->widget_2->addGraph();
    ui->widget_2->graph()->setData(k, d);
    ui->widget_2->xAxis->setLabel("t");
    ui->widget_2->yAxis->setLabel("n");
    ui->widget_2->xAxis->setRange(a,b);
    ui->widget_2->yAxis->setRange(o,l);
    ui->widget_2->replot();

}

void MainWindow::Runge_Kutta(){

    double  k0, k1, k2, k3;
    double *n, *t;
    int i;
    t = new double[p + 1];
    n = new double[p + 1];

    QVector<double> k(p+1), d(p+1);
    t[0] = 0; n[0] = 0;
    for (i = 1; i <= p; i++){
        t[i] = a + i*h;
        k0 =f(t[i-1], n[i-1]);
        k1 =f(t[i-1] + h / 2, n[i-1] + k0*h / 2);
        k2 =f(t[i-1] + h / 2, n[i-1]+ k1 *h/ 2);
        k3 =f(t[i-1] + h, n[i-1] + k2*h);
        n[i]=n[i-1]+h/6*(k0 + 2 * k1 + 2 * k2 + k3);
        k[i]=t[i];
        d[i]=n[i];
    }
    ui->widget_3->addGraph();
    ui->widget_3->graph()->setData(k, d);
    ui->widget_3->xAxis->setLabel("t");
    ui->widget_3->yAxis->setLabel("n");
    ui->widget_3->xAxis->setRange(a,b);
    ui->widget_3->yAxis->setRange(o,l);
    ui->widget_3->replot();

}

void MainWindow::Adams_Bash(){

    double *n, *t, *del;
    int i;
    t = new double[p + 1];
    n = new double[p + 1];
    del = new double[p + 1];

    QVector<double> k(p+1), d(p+1);

    t[0] = 0; n[0] = 0;
    for (i = 1; i <= p; i++){
        t[i] = a + i*h;
        k[i]=t[i];
        if (i <= 3){
            n[i] = n[i - 1] + h*f(t[i - 1], n[i - 1]);
            del[i] = 0;
            d[i]=n[i];
        }
        else{
            del[i] = h / 24 * (55 * f(t[i - 1], n[i - 1]) - 59 * f(t[i - 2], n[i - 2]) + 37 * f(t[i - 3], n[i - 3]) - 9 * f(t[i - 4], n[i - 4]));
            n[i] = n[i - 1] + del[i];
            d[i]=n[i];
        }
    }
    ui->widget_4->addGraph();
    ui->widget_4->graph()->setData(k, d);
    ui->widget_4->xAxis->setLabel("t");
    ui->widget_4->yAxis->setLabel("n");
    ui->widget_4->xAxis->setRange(a,b);
    ui->widget_4->yAxis->setRange(o,l);
    ui->widget_4->replot();

}

void MainWindow::Adams_Moul(){
    double *n, *t,*c;
    int i;
    t = new double[p + 1];
    n = new double[p + 1];
    c = new double[p + 1];

    QVector<double> k(p+1), d(p+1);

    t[0] = 0; n[0] = 0;
    for (i = 1; i <= p; i++){
    t[i] = a + i*h;
    k[i]=t[i];
    if (i <= 3){
        n[i] = n[i - 1] + h*f(t[i - 1], n[i - 1]);
        d[i]=n[i];
    }
    else{
        n[i] = n[i - 1] +h / 24 * (55 * f(t[i - 1], n[i - 1]) - 59 * f(t[i - 2], n[i - 2]) + 37 * f(t[i - 3], n[i - 3]) - 9 * f(t[i - 4], n[i - 4]));
        c[i] = n[i - 1] + h / 12 * (9 * f(t[i], n[i]) + 19 * f(t[i - 1], n[i - 1]) - 5 * f(t[i - 2], n[i - 2]) + f(t[i - 3], n[i - 3]));
        d[i]=c[i];
    }
    }
    ui->widget_5->addGraph();
    ui->widget_5->graph()->setData(k, d);
    ui->widget_5->xAxis->setLabel("t");
    ui->widget_5->yAxis->setLabel("n");
    ui->widget_5->xAxis->setRange(a,b);
    ui->widget_5->yAxis->setRange(o,l);
    ui->widget_5->replot();
}

void MainWindow::BDF(){
    double *n, *t,*c;
    int i;
    t = new double[p + 1];
    n = new double[p + 1];
    c = new double[p + 1];

    QVector<double> k(p+1), d(p+1);

    t[0] = 0; n[0] = 0;
    for (i = 1; i <= p; i++){
        t[i] = a + i*h;
        k[i]=t[i];
        if (i <= 4){
            n[i] = n[i-1] + h*f(t[i-1], n[i-1]);
            d[i]=n[i];
        }
        else{
            n[i] = n[i - 1] +h / 24 * (55 * f(t[i - 1], n[i - 1]) - 59 * f(t[i - 2], n[i - 2]) + 37 * f(t[i - 3], n[i - 3]) - 9 * f(t[i - 4], n[i - 4]));
            c[i] =6/11* h*f(t[i], n[i]) + 18 / 17 * n[i-1] - 9 / 11* n[i -2] + 2 / 11 * n[i-3];
            d[i]=c[i];
        }
    }
    ui->widget_6->addGraph();
    ui->widget_6->graph()->setData(k, d);
    ui->widget_6->xAxis->setLabel("t");
    ui->widget_6->yAxis->setLabel("n");
    ui->widget_6->xAxis->setRange(a,b);
    ui->widget_6->yAxis->setRange(o,l);
    ui->widget_6->replot();
}

void MainWindow::on_pushButton_clicked()
{
    P_Eiler();
    V_Eiler();
    Runge_Kutta();
    Adams_Bash();
    Adams_Moul();
    BDF();
}
