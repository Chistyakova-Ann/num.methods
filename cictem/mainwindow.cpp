#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

double Va=0.9e8;
double Vd=2.7e5;
double Bep=1e-13;
double W=1e7;
double Bnp=1e-12;
double a=0;
double b=33e-9;
double h=1e-8;
int p = (int)((b - a) / h);
const double pi=3.141592653589793238463;
double e=-10;
double j=20;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

double MainWindow::fi1(double t,double x){
  double  rt=2e-9;
  double  rg=1e-8;
   return (1/(rt*sqrt(2*pi))*exp(-((t-rg)*(t-rg))/(2*rt*rt)));

}

double MainWindow::f1(double t,double x, double y, double z){
   return (fi1(t,x)-Va)*x+Vd*z-Bep*x*y+W;
}

double MainWindow::f2(double t,double x, double y, double z){
   return  fi1(t,x)*x-Bep*x*y-Bnp*y*z+W;
}

double MainWindow::f3(double t,double x, double y, double z){
   return Va*x-Vd*z-Bnp*z*y+t*0;
}

void MainWindow::P_Eiler(){

    double *x,*y,*z,*t;
    int i;
    t= new double[p + 1];
    x= new double[p + 1];
    y= new double[p + 1];
    z= new double[p + 1];

    QVector<double> k(p+1), d(p+1);
    QVector<double> l(p+1), m(p+1);
    QVector<double> r(p+1), q(p+1);


    t[0] = 0; y[0] = 10^9;z[0] = 10^9;x[0] = 10^4;
    for (i = 1; i <= p; i++){
        t[i] = a + i*h;
        k[i]=t[i];
        l[i]=t[i];
        r[i]=t[i];

        x[i] = x[i - 1] + h*f1(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
        y[i] = y[i - 1] + h*f2(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
        z[i] = z[i - 1] + h*f3(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);

        d[i]=x[i];
        m[i]=y[i];
        q[i]=z[i];
    }
    ui->widget->addGraph();
    ui->widget->graph(0)->setData(k, d);

    ui->widget->addGraph();
    ui->widget->graph(1)->setPen(QPen(Qt::red));
    ui->widget->graph(1)->setData(l,m);

    ui->widget->addGraph();
    ui->widget->graph(2)->setPen(QPen(Qt::green));
    ui->widget->graph(2)->setData(r,q);

    ui->widget->xAxis->setLabel("t");
    ui->widget->yAxis->setLabel("n");
    ui->widget->xAxis->setRange(a,b);
    ui->widget->yAxis->setRange(e,j);
    ui->widget->replot();
}

void MainWindow::V_Eiler(){
    double *x,*y,*z,*t,*c,*d,*g;
    int i;
    t= new double[p + 1];
    x= new double[p + 1];
    y= new double[p + 1];
    z= new double[p + 1];
    c= new double[p + 1];
    d= new double[p + 1];
    g= new double[p + 1];

    QVector<double> k(p+1), da(p+1);
    QVector<double> l(p+1), m(p+1);
    QVector<double> r(p+1), q(p+1);

    t[0] = 0;y[0] = 10^9;z[0] = 10^9;x[0] = 10^4;
    for (i = 1; i <= p; i++){
        t[i] = a + i*h;
        k[i]=t[i];
        l[i]=t[i];
        r[i]=t[i];
        if(i<2){
            x[i] = x[i - 1] + h*f1(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            y[i] = y[i - 1] + h*f2(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            z[i] = z[i - 1] + h*f3(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            da[i]=x[i];
            m[i]=y[i];
            q[i]=z[i];
        }
        else{
            x[i] = x[i - 1] + h*f1(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            y[i] = y[i - 1] + h*f2(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            z[i] = z[i - 1] + h*f3(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            c[i] = x[i - 1] + h*f1(t[i], x[i],y[i],z[i]);
            d[i] = y[i - 1] + h*f2(t[i], x[i],y[i],z[i]);
            g[i] = z[i - 1] + h*f3(t[i], x[i],y[i],z[i]);

            da[i]=c[i];
            m[i]=d[i];
            q[i]=g[i];
        }
    }
    ui->widget_2->addGraph();
    ui->widget_2->graph(0)->setData(k, da);

    ui->widget_2->addGraph();
    ui->widget_2->graph(1)->setPen(QPen(Qt::red));
    ui->widget_2->graph(1)->setData(l,m);

    ui->widget_2->addGraph();
    ui->widget_2->graph(2)->setPen(QPen(Qt::green));
    ui->widget_2->graph(2)->setData(r,q);

    ui->widget_2->xAxis->setLabel("t");
    ui->widget_2->yAxis->setLabel("n");
    ui->widget_2->xAxis->setRange(a,b);
    ui->widget_2->yAxis->setRange(e,j);
    ui->widget_2->replot();

}

void MainWindow::Runge_Kutta(){
    double  k0x, k1x, k2x, k3x;
    double  k0y, k1y, k2y, k3y;
    double  k0z, k1z, k2z, k3z;

    double *x,*y,*z,*t;
    int i;
    t= new double[p + 1];
    x= new double[p + 1];
    y= new double[p + 1];
    z= new double[p + 1];

    QVector<double> k(p+1), d(p+1);
    QVector<double> l(p+1), m(p+1);
    QVector<double> r(p+1), q(p+1);

    t[0] = 0;y[0] = 10^9;z[0] = 10^9;x[0] = 10^4;
    for (i = 1; i <= p; i++){
        t[i] = a + i*h;
        k[i]=t[i];
        l[i]=t[i];
        r[i]=t[i];

        k0x = f1(t[i-1],x[i - 1],y[i - 1], z[i - 1]);
        k1x = f1(t[i-1] + h / 2, x[i-1] + k0x*h / 2,y[i-1] + k0x*h / 2,z[i-1] + k0x*h / 2);
        k2x = f1(t[i-1] + h / 2, x[i-1] + k1x*h / 2,y[i-1] + k1x*h / 2,z[i-1] + k1x*h / 2);
        k3x = f1(t[i-1] + h, x[i-1] + k2x*h ,y[i-1] + k2x*h,z[i-1] + k2x*h);

        k0y = f2(t[i-1],x[i - 1],y[i - 1], z[i - 1]);
        k1y = f2(t[i-1] + h / 2, x[i-1] + k0y*h / 2,y[i-1] + k0y*h / 2,z[i-1] + k0y*h / 2);
        k2y = f2(t[i-1] + h / 2, x[i-1] + k1y*h / 2,y[i-1] + k1y*h / 2,z[i-1] + k1y*h / 2);
        k3y = f2(t[i-1] + h, x[i-1] + k2y*h ,y[i-1] + k2y*h,z[i-1] + k2y*h);

        k0z = f3(t[i-1],x[i - 1],y[i - 1], z[i - 1]);
        k1z = f3(t[i-1] + h / 2, x[i-1] + k0z*h / 2,y[i-1] + k0z*h / 2,z[i-1] + k0z*h / 2);
        k2z = f3(t[i-1] + h / 2, x[i-1] + k1z*h / 2,y[i-1] + k1z*h / 2,z[i-1] + k1z*h / 2);
        k3z = f3(t[i-1] + h, x[i-1] + k2z*h ,y[i-1] + k2z*h,z[i-1] + k2z*h);

        x[i]=x[i-1]+h/6*(k0x + 2 * k1x + 2 * k2x + k3x);
        y[i]=y[i-1]+h/6*(k0y + 2 * k1y + 2 * k2y + k3y);
        z[i]=z[i-1]+h/6*(k0z + 2 * k1z + 2 * k2z + k3z);

        d[i]=x[i];
        m[i]=y[i];
        q[i]=z[i];
    }
    ui->widget_5->addGraph();
    ui->widget_5->graph(0)->setData(k, d);

    ui->widget_5->addGraph();
    ui->widget_5->graph(1)->setPen(QPen(Qt::red));
    ui->widget_5->graph(1)->setData(l,m);

    ui->widget_5->addGraph();
    ui->widget_5->graph(2)->setPen(QPen(Qt::green));
    ui->widget_5->graph(2)->setData(r,q);

    ui->widget_5->xAxis->setLabel("t");
    ui->widget_5->yAxis->setLabel("n");
    ui->widget_5->xAxis->setRange(a,b);
    ui->widget_5->yAxis->setRange(e,j);
    ui->widget_5->replot();

}

void MainWindow::Adams_Bash() {

    double *x,*y,*z,*t;
    int i;
    int p = (int)((b - a) / h);
    t = new double[p + 1];
    x= new double[p + 1];
    y= new double[p + 1];
    z= new double[p + 1];

    QVector<double> k(p+1), d(p+1);
    QVector<double> l(p+1), m(p+1);
    QVector<double> r(p+1), q(p+1);

    t[0] = 0;y[0] = 10^9;z[0] = 10^9;x[0] = 10^4;
    for (i = 1; i <= p; i++){
        t[i] = a + i*h;

        k[i]=t[i];
        l[i]=t[i];
        r[i]=t[i];

        if (i <= 3){
            x[i] = x[i - 1] + h*f1(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            y[i] = y[i - 1] + h*f2(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            z[i] = z[i - 1] + h*f3(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);

            d[i]=x[i];
            m[i]=y[i];
            q[i]=z[i];
        }
        else{
            x[i] = x[i - 1] + h / 24 * (55 * f1(t[i - 1], x[i - 1],y[i - 1],z[i - 1]) - 59 * f1(t[i - 2], x[i - 2],y[i - 2],z[i - 2]) + 37 * f1(t[i - 3], x[i - 3],y[i - 3],z[i - 3]) - 9 * f1(t[i - 4], x[i - 4],y[i - 4],z[i - 4]));
            y[i] = y[i - 1] + h / 24 * (55 * f2(t[i - 1], x[i - 1],y[i - 1],z[i - 1]) - 59 * f2(t[i - 2], x[i - 2],y[i - 2],z[i - 2]) + 37 * f2(t[i - 3], x[i - 3],y[i - 3],z[i - 3]) - 9 * f2(t[i - 4], x[i - 4],y[i - 4],z[i - 4]));
            z[i] = z[i - 1] + h / 24 * (55 * f3(t[i - 1], x[i - 1],y[i - 1],z[i - 1]) - 59 * f3(t[i - 2], x[i - 2],y[i - 2],z[i - 2]) + 37 * f3(t[i - 3], x[i - 3],y[i - 3],z[i - 3]) - 9 * f3(t[i - 4], x[i - 4],y[i - 4],z[i - 4]));

            d[i]=x[i];
            m[i]=y[i];
            q[i]=z[i];

        }
    }
    ui->widget_3->addGraph();
    ui->widget_3->graph(0)->setData(k, d);

    ui->widget_3->addGraph();
    ui->widget_3->graph(1)->setPen(QPen(Qt::red));
    ui->widget_3->graph(1)->setData(l,m);

    ui->widget_3->addGraph();
    ui->widget_3->graph(2)->setPen(QPen(Qt::green));
    ui->widget_3->graph(2)->setData(r,q);

    ui->widget_3->xAxis->setLabel("t");
    ui->widget_3->yAxis->setLabel("n");
    ui->widget_3->xAxis->setRange(a,b);
    ui->widget_3->yAxis->setRange(e,j);
    ui->widget_3->replot();
}

void MainWindow::Adams_Moul() {

    double *x,*y,*z,*t,*c,*d,*g;
    int i;
    t= new double[p + 1];
    x= new double[p + 1];
    y= new double[p + 1];
    z= new double[p + 1];
    c= new double[p + 1];
    d= new double[p + 1];
    g= new double[p + 1];

    QVector<double> k(p+1), da(p+1);
    QVector<double> l(p+1), m(p+1);
    QVector<double> r(p+1), q(p+1);

    t[0] = 0;y[0] = 10^9;z[0] = 10^9;x[0] = 10^4;

    for (i = 1; i <= p; i++){
        t[i] = a + i*h;
        k[i]=t[i];
        l[i]=t[i];
        r[i]=t[i];
        if (i <= 3){
            x[i] = x[i - 1] + h*f1(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            y[i] = y[i - 1] + h*f2(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            z[i] = z[i - 1] + h*f3(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);

            da[i]=x[i];
            m[i]=y[i];
            q[i]=z[i];

        }
        else{
            x[i] = x[i - 1] + h / 24 * (55 * f1(t[i - 1], x[i - 1],y[i - 1],z[i - 1]) - 59 * f1(t[i - 2], x[i - 2],y[i - 2],z[i - 2]) + 37 * f1(t[i - 3], x[i - 3],y[i - 3],z[i - 3]) - 9 * f1(t[i - 4], x[i - 4],y[i - 4],z[i - 4]));
            y[i] = y[i - 1] + h / 24 * (55 * f2(t[i - 1], x[i - 1],y[i - 1],z[i - 1]) - 59 * f2(t[i - 2], x[i - 2],y[i - 2],z[i - 2]) + 37 * f2(t[i - 3], x[i - 3],y[i - 3],z[i - 3]) - 9 * f2(t[i - 4], x[i - 4],y[i - 4],z[i - 4]));
            z[i] = z[i - 1] + h / 24 * (55 * f3(t[i - 1], x[i - 1],y[i - 1],z[i - 1]) - 59 * f3(t[i - 2], x[i - 2],y[i - 2],z[i - 2]) + 37 * f3(t[i - 3], x[i - 3],y[i - 3],z[i - 3]) - 9 * f3(t[i - 4], x[i - 4],y[i - 4],z[i - 4]));
            c[i] = x[i - 1] + h / 12 * (9 * f1(t[i], x[i],y[i],z[i]) + 19 * f1(t[i - 1], x[i-1],y[i-1],z[i-1]) - 5 * f1(t[i - 2],x[i-2],y[i-2],z[i-2]) + f1(t[i - 3], x[i-3],y[i-3],z[i-3]));
            d[i] = y[i - 1] + h / 12 * (9 * f2(t[i], x[i],y[i],z[i]) + 19 * f2(t[i - 1], x[i-1],y[i-1],z[i-1]) - 5 * f2(t[i - 2],x[i-2],y[i-2],z[i-2]) + f2(t[i - 3], x[i-3],y[i-3],z[i-3]));
            g[i] = z[i - 1] + h / 12 * (9 * f3(t[i], x[i],y[i],z[i]) + 19 * f3(t[i - 1], x[i-1],y[i-1],z[i-1]) - 5 * f3(t[i - 2],x[i-2],y[i-2],z[i-2]) + f3(t[i - 3], x[i-3],y[i-3],z[i-3]));

            da[i]=c[i];
            m[i]=d[i];
            q[i]=g[i];
        }
    }
    ui->widget_4->addGraph();
    ui->widget_4->graph(0)->setData(k, da);

    ui->widget_4->addGraph();
    ui->widget_4->graph(1)->setPen(QPen(Qt::red));
    ui->widget_4->graph(1)->setData(l,m);

    ui->widget_4->addGraph();
    ui->widget_4->graph(2)->setPen(QPen(Qt::green));
    ui->widget_4->graph(2)->setData(r,q);

    ui->widget_4->xAxis->setLabel("t");
    ui->widget_4->yAxis->setLabel("n");
    ui->widget_4->xAxis->setRange(a,b);
    ui->widget_4->yAxis->setRange(e,j);
    ui->widget_4->replot();
}

void MainWindow::BDF() {

    double *x,*y,*z,*t,*c,*d,*g;
    int i;
    t= new double[p + 1];
    x= new double[p + 1];
    y= new double[p + 1];
    z= new double[p + 1];
    c= new double[p + 1];
    d= new double[p + 1];
    g= new double[p + 1];

    QVector<double> k(p+1), da(p+1);
    QVector<double> l(p+1), m(p+1);
    QVector<double> r(p+1), q(p+1);

    t[0] = 0;y[0] = 10^9;z[0] = 10^9;x[0] = 10^4;
    for (i = 1; i <= p; i++){
        t[i] = a + i*h;
        k[i]=t[i];
        l[i]=t[i];
        r[i]=t[i];
        if (i <= 4){
            x[i] = x[i - 1] + h*f1(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            y[i] = y[i - 1] + h*f2(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);
            z[i] = z[i - 1] + h*f3(t[i - 1], x[i - 1],y[i - 1], z[i - 1]);

            da[i]=x[i];
            m[i]=y[i];
            q[i]=z[i];
        }
         else{
            x[i] = x[i - 1] + h / 24 * (55 * f1(t[i - 1], x[i - 1],y[i - 1],z[i - 1]) - 59 * f1(t[i - 2], x[i - 2],y[i - 2],z[i - 2]) + 37 * f1(t[i - 3], x[i - 3],y[i - 3],z[i - 3]) - 9 * f1(t[i - 4], x[i - 4],y[i - 4],z[i - 4]));
            y[i] = y[i - 1] + h / 24 * (55 * f2(t[i - 1], x[i - 1],y[i - 1],z[i - 1]) - 59 * f2(t[i - 2], x[i - 2],y[i - 2],z[i - 2]) + 37 * f2(t[i - 3], x[i - 3],y[i - 3],z[i - 3]) - 9 * f2(t[i - 4], x[i - 4],y[i - 4],z[i - 4]));
            z[i] = z[i - 1] + h / 24 * (55 * f3(t[i - 1], x[i - 1],y[i - 1],z[i - 1]) - 59 * f3(t[i - 2], x[i - 2],y[i - 2],z[i - 2]) + 37 * f3(t[i - 3], x[i - 3],y[i - 3],z[i - 3]) - 9 * f3(t[i - 4], x[i - 4],y[i - 4],z[i - 4]));

            c[i] =6/11* h*f1(t[i], x[i],y[i],z[i]) + 18 / 17 *(x[i-1]) - 9 / 11*x[i-2] + 2 / 11 *x[i-3];
            d[i] =6/11* h*f2(t[i], x[i],y[i],z[i]) + 18 / 17 *(y[i-1]) - 9 / 11*y[i-2] + 2 / 11 *y[i-3];
            g[i] =6/11* h*f3(t[i], x[i],y[i],z[i]) + 18 / 17 *(z[i-1]) - 9 / 11*z[i-2] + 2 / 11 *z[i-3];

            da[i]=c[i];
            m[i]=d[i];
            q[i]=g[i];

        }

    }
    ui->widget_6->addGraph();
    ui->widget_6->graph(0)->setData(k, da);

    ui->widget_6->addGraph();
    ui->widget_6->graph(1)->setPen(QPen(Qt::red));
    ui->widget_6->graph(1)->setData(l,m);

    ui->widget_6->addGraph();
    ui->widget_6->graph(2)->setPen(QPen(Qt::green));
    ui->widget_6->graph(2)->setData(r,q);

    ui->widget_6->xAxis->setLabel("t");
    ui->widget_6->yAxis->setLabel("n");
    ui->widget_6->xAxis->setRange(a,b);
    ui->widget_6->yAxis->setRange(e,j);
    ui->widget_6->replot();

}

void MainWindow::on_pushButton_clicked()
{
    P_Eiler();
    V_Eiler();
    Runge_Kutta();
    Adams_Bash();
    Adams_Moul();
    BDF();
    ui->label_7->setPixmap(QPixmap("/home/anna/num.methods/cictem/ter.jpg"));
}
