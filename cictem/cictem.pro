#-------------------------------------------------
#
# Project created by QtCreator 2018-03-13T18:27:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cictem
TEMPLATE = app
QT       += printsupport


SOURCES += main.cpp\
        mainwindow.cpp\
        qcustomplot.cpp

HEADERS  += mainwindow.h\
            qcustomplot.h \


FORMS    += mainwindow.ui
