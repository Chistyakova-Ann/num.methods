#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    double fi1(double t,double x);
    double f1(double t,double x, double y, double z);
    double  f2(double t,double x, double y, double z);
    double f3(double t,double x, double y, double z);
    void P_Eiler();
    void V_Eiler();
    void Runge_Kutta();
    void Adams_Bash();
    void Adams_Moul();
    void BDF();



private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
