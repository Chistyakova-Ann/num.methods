%{
#include "bison_cal.tab.c"
%}

%option noyywrap
%option always-interactive

%%
[ \t]  {}

([0-9]+|[0-9]+"."[0-9]*)     { yylval.number = atof(yytext); return NUMBER; }

sin|cos|tan|atan|asin|acos|log|exp|sqrt|abs { yylval.var =  new string(yytext); return FUNC;}

[a-zA-Z][a-zA-Z0-9]*      { yylval.var = new string(yytext); return VAR; }

[-|+|*|/|\n|=|(|)|^]      { return *yytext; }

.          yyerror("Unknown character");
%%

int main() 
{	

	yyparse();
	return 0;
}
